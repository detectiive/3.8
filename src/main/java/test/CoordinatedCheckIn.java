package test;

import java.time.LocalTime;

public class CoordinatedCheckIn {
    public static final Object LOCK = new Object();

    public static void checkIn() {
        try {
            synchronized (LOCK) {
                LOCK.wait();
                System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(LocalTime.now() + ": Початок виконання");
        new Thread(CoordinatedCheckIn::checkIn, "AAPL").start();
        new Thread(CoordinatedCheckIn::checkIn, "COKE").start();
        new Thread(CoordinatedCheckIn::checkIn, "IBM").start();
        for (int i = 0; i < 3; i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (LOCK) {
                LOCK.notify();
            }
        }
    }
}
