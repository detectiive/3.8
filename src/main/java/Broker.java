import java.util.List;

public class Broker {
    public String brokerName;
    List<Target> targetList;

    public Broker(String brokerName, List<Target> targetList) {
        this.brokerName = brokerName;
        this.targetList = targetList;
    }

}
