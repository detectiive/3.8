import java.time.LocalTime;

public class test1 {

    public static void main(String[] args) {
        System.out.println("start");
        int n = 100;
        for (int i = 0; i < 30; i++) {
            int min = (int) (n - n * 0.03); // Minimum
            int max = (int) (n + n * 0.03); // Maximum
            int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
            System.out.println(LocalTime.now()+" "+random_int);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
