import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Exchange {
    static Stock stockAAPL = new Stock("AAPL", 100, 141);
    static Stock stockCOKE = new Stock("COKE", 1000, 387);
    static Stock stockIBM = new Stock("IBM", 200, 137);


    public static void main(String[] args) throws InterruptedException {
        System.out.println(LocalTime.now() + ": Початок виконання");
        stockAAPL.range();
        stockCOKE.range();
        stockIBM.range();
        TimeUnit.SECONDS.sleep(3);
        System.out.println("Число: " + stockAAPL.price);
    }
}
