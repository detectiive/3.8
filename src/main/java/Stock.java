import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Stock {
    public String name;
    public int amount;
    public int price;

    public Stock(String name, int amount, int price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public void printStockDetails() {
        synchronized (this) {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + " Name - " + this.name);
            System.out.println(threadName + " Amount - " + this.amount);
            System.out.println(threadName + " Price - " + this.price);
        }
    }

    void range() {
        new Thread(() -> {
            try {
                for (int i = 0; i < 20; i++) {
                    String time = LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME);
                    int n = price;
                    int min = (int) (n - n * 0.03); // Minimum
                    int max = (int) (n + n * 0.03); // Maximum
                    int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
                    price = random_int;
                    System.out.println(time + " Ціна акцій компанії " + name + " змінилась. Поточна вартість: " + random_int);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "1").start();
    }
}

